\chapter{Démonstration}

\section{Contexte}
Dans cette démonstration, nous allons essayer d’utiliser au maximum les fonctionnalités mises à disposition par GitLab CI/CD. 
Nous serons toutefois limités car nous ne bénéficions pas d’un abonnement « Bronze Plan », « Silver Plan » ou « Gold Plan » mais du « Free Plan » 
avec un nombre de fonctionnalités limité.

La démonstration se fera dans le contexte d’une application (très simple) développées en Java, le tout utilisant l’environnement 
de développement intégré Eclipse et Gradle, un outil pour automatiser la construction de projets Java (ainsi que d’autres langages comme Scala, Groovy, etc.). 
Gradle est un outil extensible très pratique qui nous permet d’importer différents modules déjà existants comme JaCoCo, et même de créer des scripts personnalisés 
facilitant la mise en place de nouvelles tâches pour notre intégration continue. Gradle est un outil orienté sur des actions directement en ligne de commande, 
ce qui nous permettra d’automatiser de nombreuses tâches au moment de la mise en place de l’intégration continue.


\section{Création du projet}

La première étape est de créer un projet avec GitLab. On définit un nom de projet ainsi que sa visibilité. On peut aussi ajouter une description 
et l’initialiser avec un fichier README.

Une fois le projet GitLab créé, nous l’avons cloné en local puis nous avons utilisé Eclipse pour créer un nouveau projet en utilisant Gradle.

Notre projet comprend trois classes simples, décrites dans le tableau \ref{tab:classes}.

\begin{table}[H]
  \caption{Les classes du projet}
  \begin{center}
    \begin{tabular}{|p{2in}|p{2in}|}
      \hline
      Classe & Méthodes \\
      \hline\hline
      Calculatrice & \begin{tabular}{@{}c@{}}int Addition(int a, int b) \\ int Soustraction(int a, int b) \\ int Multiplication(int a, int b) \\ float Division(int a, int b)\end{tabular}  \\
      \hline
      CalculatriceException &  méthodes héritées de java.lang.Exception \\
      \hline
      DividedByZeroException &  méthodes héritées de CalculatriceException \\
      \hline
  \end{tabular}
  \label{tab:classes}
  \end{center}
\end{table}


Il comprend aussi une classe de test pour les méthodes de Calculatrice. Nous utilisons le framework JUnit 4 pour l’exécution des tests unitaires. 
Les tests pour la classe Calculatrice sont décris dans le tableau \ref{tab:tests}.

\begin{table}[H]
  \caption{Tests contenus par la classe de test CalculatriceTest}
  \begin{center}
    \begin{tabular}{|p{2in}|p{2in}|}
      \hline
      Nom du test & But du test \\
      \hline\hline
      testDividedByZero & Test de la méthode “Division”, on vérifie que la méthode renvoie “DividedByZeroException “ en cas de division par zéro.  \\
      \hline
      \makecell[{{p{2in}}}]{ testDivision \\ testAddition \\ testSoustration \\ testMultiplication } & Vérification que la méthode renvoie la bonne réponse quand les paramètres sont valides. \\
      \hline
  \end{tabular}
  \label{tab:tests}
  \end{center}
\end{table}

\section{Ajout des tâches sur Gradle}

Afin de pouvoir avoir la couverture de notre projet nous avons utilisé l’outil JaCoCo. Pour cela nous avons modifié le fichier build.gradle à l’aide d’une 
configuration que l’on a trouvé sur GitHub \cite{gradlejacoco}.
Gradle agit comme un outil de gestion de dépendances, on commence donc par ajouter JaCoCo dans la liste des plugins. On configure alors la tâche de couverture. Dans notre projet nous avons choisi de tester la couverture des méthodes.
JaCoCo va compter le nombre de méthodes couvertes et manquées. Via un script Gradle, on va récupérer les valeurs et on va calculer la couverture totale en suivant la formule suivante:

\begin{itemize}
  \item Couverture = Covered / (Covered + Missed ) * 100
\end{itemize}

Le résultat est alors affiché dans le terminal, lorsque la commande « gradle coverageReport » est lancée.

\section{Mise en place de GitLab CI/CD}

Avant la mise en place de l’outil, nous devons réfléchir à ce que l’on veut automatiser pour ce projet.

Premièrement, on veut automatiser la construction d’un fichier .jar qui va contenir nos classes précédemment créées pour qu’elles puissent être utilisées dans un autre projet.

On souhaite aussi lancer nos tests unitaires et être prévenu si un ou plusieurs tests ne passent pas. Cela est également souhaitable de savoir le pourcentage de couverture du code selon une métrique choisie.

Notre projet étant open source, on souhaite encourager d’autres développeurs à contribuer ou utiliser le projet,
il est donc important d’avoir un code bien commenté.
Une fois le code commenté, on veut mettre en ligne un site web avec notre documentation JavaDoc pour éviter aux autres de devoir lire directement la documentation dans le code source.

Enfin, lorsqu’une nouvelle version de notre projet est créée, on souhaite aussi automatiser la création de « tags » Git portant le nom de la nouvelle version du projet (par exemple « v1.1-rc1 »).

Cette automatisation est matérialisée par le fichier « .gitlab-ci.yml » que nous avons écrit et pour lequel nous allons détailler le rôle de chaque section.

Nous indiquons au tout début du fichier que nous utilisons l’image Docker la plus récente de Gradle, qui utilise la distribution Linux Alpine (une distribution Linux légère). 

\begin{lstlisting}[caption=Portion 1 du fichier .gitlab-ci.yml,language=yaml]
image : gradle:alpine
\end{lstlisting}

Cela nous permet de bénéficier d’un environnement où tous les outils nécessaires à l’automatisation sont installés (Java, Gradle, etc.) et accessibles en ligne de commande.

Ensuite, nous pouvons configurer les variables d’environnement qui seront accessibles lors de l’exécution des tâches. Ici, nous configurons une option pour Gradle afin d’interdire la création d’un démon. Par défaut, Gradle démarre un démon pour accélérer l’exécution des tâches Gradle suivantes car cela évite de démarrer Gradle à chaque fois. C’est une option que nous ne souhaitons pas dans le contexte de l’intégration continue car nous ne souhaitons pas réutiliser les instances précédentes de Gradle qui ont été utilisées lors d’une construction précédente. 

\begin{lstlisting}[caption=Portion 2 du fichier .gitlab-ci.yml,language=yaml]
variables:
  GRADLE_OPTS: "-Dorg.gradle.daemon=false"
\end{lstlisting}

Nous listons maintenant les stages de notre pipeline.
Ils sont au nombre de quatre et seront lancé les uns après les autres, il y a d’abord l’étape de construction (« build »),
ensuite l’exécution des tests (« test »), le déploiement de la documentation (« deploy ») et enfin la création d’une nouvelle « release », qui est une étape manuelle. Si une tâche d’un stage échoue, le pipeline entier est arrêté (les stages suivants ne sont pas lancés) et le pipeline est marqué comme « échoué ». Par défaut lors de l’échec, GitLab envoie un courriel au développeur qui a déclenché le pipeline pour le prévenir qu’une erreur est survenue.

\begin{lstlisting}[caption=Portion 3 du fichier .gitlab-ci.yml,language=yaml]
stages:
  - build
  - test
  - deploy
  - release
\end{lstlisting}

Le mot clé « before\_script » nous permet d’indiquer une commande qui sera exécutée avant toutes les autres commandes de chaque tâche.
Cela est par exemple utile pour définie une variable d’environnement dont la valeur nécessite l’exécution d’une commande.
Ici, cela est utilisé pour définir la variable « GRADLE\_USER\_HOME » avec une valeur qui dépend du répertoire où la commande est exécutée.

\begin{lstlisting}[caption=Portion 4 du fichier .gitlab-ci.yml,language=yaml]
before_script:
  - export GRADLE_USER_HOME=`pwd`/.gradle
\end{lstlisting}

Cette première tâche fait partie du stage « build » et a pour rôle de construire le projet et ainsi générer notre fichier .jar. Le projet est compilé en une seule commande grâce à Gradle et le fichier .jar est envoyé à GitLab grâce au mot clé « artifacts ». Cet artéfact est défini avec une validité de 7 jours et sera supprimé une fois les 7 jours passés, cela est utile pour ne pas garder indéfiniment des fichiers .jar qui sont générés très régulièrement. Un aspect à noter est la possibilité de mettre en cache certains dossiers, pour qu’ils puissent être réutilisés lors des prochaines tâches, cela est effectué grâce au mot clé « cache ». Il existe un cache par branche, car la clé est définie à « \$CI\_COMMIT\_REF\_NAME », qui est une variable d’environnement qui contient le nom de la branche actuelle.
Cela nous évite de recompiler des fichiers déjà compilés ou de retélécharger des dépendances déjà mises en cache.

\begin{lstlisting}[caption=Portion 5 du fichier .gitlab-ci.yml,language=yaml]
build:
  stage: build
  script: gradle --build-cache assemble
  artifacts:
    paths:
      - build/libs/*.jar
    expire_in: 1 week
  cache:
    key: "$CI_COMMIT_REF_NAME"
    policy: push
    paths:
      - build
      - .gradle
\end{lstlisting}


Nous passons ensuite à la prochaine étape, le lancement des tests unitaires et le calcul de la couverture. Le projet ne sera pas recompilé car les fichiers compilés ont été mis en cache et sont téléchargés avant l’exécution de la tâche. Les tests n’ont pas été compilés à l’étape précédente et sont donc compilés à cette étape. La ligne « gradle test » compile les tests unitaires puis les exécute afin de vérifier que les tests passent. Si un test ne passe pas, le pipeline s’arrête ici.

La ligne « gradle coverageReport » lance la tâche coverageReport que nous avons ajoutée dans notre fichier build.gradle et affiche le pourcentage de couverture dans le terminal. Ce pourcentage de couverture est capturé par GitLab selon l’expression régulière que nous allons fournir.


\begin{lstlisting}[caption=Portion 6 du fichier .gitlab-ci.yml,language=yaml]
test:
  stage: test
  script:
    - gradle test
    - gradle coverageReport
  artifacts:
    reports:
      junit: build/test-results/test/**/TEST-*.xml
  cache:
    key: "$CI_COMMIT_REF_NAME"
    policy: pull-push
    paths:
      - build
      - .gradle
\end{lstlisting}

Cette tâche va utiliser Gradle afin de générer une documentation au format HTML avec JavaDoc et ensuite la publier sur le GitLab Pages du projet (via un artéfact). Cette tâche est uniquement exécutée sur la branche master. Le dossier public est le dossier utilisé par GitLab pour récupérer le contenu du site.


\begin{lstlisting}[caption=Portion 7 du fichier .gitlab-ci.yml,language=yaml]
pages:
  stage: deploy
  only:
    - master
  artifacts:
    paths:
      - public
  script:
    - gradle javadoc
    - test -d public || mkdir public
    - cp -a build/docs/javadoc/* public/
  cache:
    key: "$CI_COMMIT_REF_NAME"
    policy: pull 
    paths:
      - build
      - .gradle
\end{lstlisting}

Dans la prochaine étape, on crée une tache optionnelle que l’utilisateur doit lancer lui-même une fois les autres étapes validées. 
Cette étape permet de générer une nouvelle release du projet grâce à un outil 
créé par des employés de GitLab \cite{releasecli}. Cette étape ne devrait s’exécuter uniquement sur la branche master et c’est pourquoi nous avons 
utilisé le mot clé « only » ici. On ne veut pas que cette étape s’exécute lors de la création d’un tag car cette tâche va justement créer des tags, 
c’est pourquoi on utilise le mot clé « except ».

Ce script demande impérativement deux variables d’environnement en paramètre (que l’on peut passer au moment du lancement manuel) :

\begin{itemize}
  \item	\$NAME : nom que l’on souhaite donner à la release (par exemple v1.1)
  \item \$MD : une description de la release (ajout, modification, etc…)
\end{itemize}

Ces paramètres sont obligatoires, sans eux cette étape échoue.

\begin{lstlisting}[caption=Portion 8 du fichier .gitlab-ci.yml,language=yaml]
release-branch:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli
  when: manual
  only:
    - master
  except:
    - tags
  script:
    - >
      ! [ -z "$NAME" ] && ! [ -z "$MD" ] &&
      release-cli create --name $NAME --description "$MD"
      --tag-name $NAME --ref $CI_COMMIT_SHA
\end{lstlisting}

A la fin, si l’on concatène tous ces blocs, on obtient un fichier que l’on peut placer à la racine de notre projet Git sous le nom « .gitlab-ci.yml ».

Pour la capture du pourcentage de couverture, il faut penser à indiquer à GitLab l’expression régulière à utiliser.
Cela se fait via l’interface web, dans l’onglet « Settings > CI/CD » du projet. 

\section{Résultats}

Une fois ces étapes effectuées, nous avons un projet dont les étapes de compilation, de tests, de déploiement (de la documentation) et de « releasing » sont entièrement automatisées et systématiques (elles s’effectuent lors de chaque « push » sur le dépôt).

Notre projet de démonstration est accessible publiquement à l’adresse suivante :
\url{https://gitlab.com/keskydi/8inf958-projet-final}


\begin{figure}
  \begin{center}
  \includegraphics[width=6in]{pipelinefinal}
  \end{center}
  \caption{Pipeline final du projet}
  \label{fig:pipelinefinal}
\end{figure}

Le pipeline que nous obtenons est celui de la figure \ref{fig:pipelinefinal}.


Il s’agit bien du pipeline que nous avons configuré précédemment, qui contient 5 tâches (dont une générée par GitLab pour le déploiement du site web statique)

La documentation du projet, générée grâce à Javadoc, est automatiquement mise à jour lorsque le code est modifié dans la branche « master ».
Cette documentation se présente sous la forme d’un site web statique accessible à l’adresse suivante :
\url{https://keskydi.gitlab.io/8inf958-projet-final/}

Il nous est possible de contrôler la visibilité du site web, en modifiant la visibilité de celui-ci dans le projet GitLab. On peut ainsi choisir de le rendre public ou de n’autoriser que les membres du projet à le consulter.

Un intérêt de l’intégration de la récupération des résultats des tests JUnit dans GitLab est que nous pouvons les consulter depuis l’interface et savoir lesquels ont échoués ou réussis.

Cela est très avantageux au moment des « merge requests » (lorsque l’on souhaite fusionner deux branches), car dans le cas où un des tests ne passe pas, GitLab pourra nous afficher quel test ne passe pas pour permettre une correction plus rapide des erreurs (figure \ref{fig:mergerequest}).

\begin{figure}
  \begin{center}
  \includegraphics[width=6in]{mergerequest}
  \end{center}
  \caption{Exemple de merge request}
  \label{fig:mergerequest}
\end{figure}