\chapter{Présentation de l’outil}

\section{Présentation de GitLab}

Avant de présenter l’outil GitLab CI/CD, nous nous devons de présenter GitLab, la plate-forme principale sur laquelle repose GitLab CI/CD.

GitLab est une plate-forme web créée en 2011 et permettant d’héberger des dépôts Git.
Chaque dépôt Git est lié à un projet, dans lequel des fonctionnalités comme la gestion des membres ayant accès au projet, 
la gestion des « Issues » (un espace de discussion permettant aux utilisateurs de la plate-forme de communiquer avec le gestionnaire du projet 
pour signaler des problèmes), et de nombreuses autres sont présentes. La plate-forme se veut ambassadrice du modèle de développement DevOps 
et offre tous les outils nécessaires pour cela (dont notamment GitLab CI/CD) \cite{gitlabdevops}.

La plate-forme offre des fonctionnalités similaires à GitHub, une autre plate-forme web d’hébergement de dépôt Git, mais on peut toutefois noter les différences suivantes :
\begin{itemize}
  \item GitLab permet une plus grande granularité sur la gestion des permissions dans un projet grâce à plusieurs niveaux de permissions basés sur des rôles. 
  Sur GitHub, seules deux permissions sont possibles : lecture seulement, ou lecture et écriture \cite{gitlabvsgithub}.
  \item GitLab est une plate-forme open-source contrairement à GitHub. L’avantage de GitLab est donc de pouvoir l’héberger sur son propre serveur et d’avoir un contrôle total sur les données hébergées. Il est toutefois possible d’utiliser l’instance communautaire disponible sur gitlab.com, et ce de façon gratuite. GitHub peut uniquement être auto-hébergé dans sa version entreprise payante.
  \item Lors de la création d’un projet, GitLab permet d’importer un dépôt depuis d’autres plates-formes ou de créer un projet à partir d’un « template » préexistant. Cela n’est pas possible sur GitHub.
  \item GitHub est une plate-forme orientée sur les interactions sociales entre utilisateurs \cite{githubsocial}, un côté que l’on retrouve moins sur la plate-forme GitLab.
\end{itemize}


Il est à noter également que GitHub a une plus grande popularité auprès de la communauté des développeurs, ce qui peut être un frein à l’adoption de la plate-forme par des développeurs ne la connaissant pas ou ayant déjà l’habitude de travailler sur une autre plate-forme.

\section{Présentation de GitLab CI/CD}

GitLab CI/CD est un outil d’intégration et de livraison continue fourni par GitLab Inc, l’entreprise qui développe GitLab.

Comme cela a pu être mentionné précédemment dans le rapport, l’outil GitLab CI/CD est directement intégré à la plate-forme GitLab, et ce depuis la version 8.8 \cite{gitlabcireleased}. Nous pouvons donc l’utiliser sans configuration spéciale au préalable.

\begin{figure}[h]
  \begin{center}
  \includegraphics[width=6in]{pipelinegitlab}
  \end{center}
  \caption{Un pipeline GitLab CI/CD}
  \label{fig:pipelinegitlab}
\end{figure}

L’outil fonctionne avec un système de « pipelines », qui consiste en une suite d’actions exécutées de manière linéaire et/ou en parallèle. 
Ces actions ne sont pas exécutées par la plate-forme GitLab, mais par des ordinateurs externes appelés « runners ». 
Il est possible d’héberger ses propres runners en utilisant le logiciel GitLab Runner, qui est un logiciel programmé avec le langage Go, 
et fonctionnant sur toutes les plates-formes principales (Windows, Linux, Mac OS). Il est à noter qu’en possédant un compte utilisateur gratuit sur GitLab, 
l’utilisateur bénéficie de 1000 minutes d’utilisation des runners hébergés par GitLab, ce qui évite de devoir héberger ses runners soi-même.

Un pipeline est séparé en plusieurs « stages », par exemple « Build », « Test », etc. Les tâches de chaque stage sont exécutées en parallèle. Il est parfois souhaitable de vérifier manuellement le statut d’un pipeline, et c’est pourquoi il aussi possible de définir des tâches qui doivent être lancées manuellement comme la tâche « deploy to production » dans la figure \ref{fig:pipelinegitlab}. 

Nous pouvons voir le statut de nos pipelines directement depuis l’interface web de GitLab, et cela sous la même forme que la figure \ref{fig:pipelinegitlab} qui est une capture d’écran du site web.

\section{Fonctionnalités et intégrations}

GitLab CI/CD est un outil constamment mis à jour, avec des ajouts de fonctionnalités très régulièrement.
Nous pouvons toutefois noter que les fonctionnalités suivantes existent au moment de l’écriture de ce rapport,
certaines fonctionnalités nécessitent un abonnement payant à GitLab et cela est précisé avec le symbole \$ entre parenthèses lorsque c’est le cas  \cite{cicddoc} :
Auto DevOps, ChatOps, Browser Performance Testing, CI services, Code Quality (\$), GitLab CI/CD for external repositories (\$),
Interactive Web Terminals (\$), JUnit tests, Using Docker images, Auto Deploy, Building Docker images, Canary Deployments (\$), Deploy Boards (\$),
Feature Flags (\$), GitLab Pages, GitLab Releases, Review Apps, Cloud Deployment, Container Scanning (\$), Dependency Scanning (\$), Licence Compliance (\$)
et Security Test Reports (\$).

Avec toutes ces fonctionnalités, on constate que GitLab CI/CD, et GitLab en général, sont orientés sur l’automatisation de nombreuses tâches, notamment en ce qui concerne les tests, la vérification et le déploiement, et vont même plus loin en proposant des fonctionnalités d’analyse de la sécurité de l’application.

Malheureusement, la plupart de ces fonctionnalités ne sont pas accessibles avec la version gratuite de GitLab, 
il faut passer sur un compte utilisateur payant pour en bénéficier. Cela fonctionne avec un système d’abonnement mensuel 
et plusieurs offres sont disponibles avec plusieurs niveaux de fonctionnalités offerts \cite{gitlabpricing}.

\section{Système de configuration}

La configuration de GitLab CI/CD se fait sous deux formes : la première est directement sur l’interface web de GitLab, 
la seconde s’effectue dans des fichiers de configuration YAML.

\subsection{Configuration YAML}

La configuration de GitLab CI/CD se fait principalement via un fichier YAML. Pour créer un pipeline, 
ce fichier doit être placé à la racine du dépôt Git et être nommé « .gitlab-ci.yml ».

Si le fichier n’existe pas et que la fonctionnalité « Auto DevOps » est activée, il sera tout de même possible d’utiliser GitLab CI/CD 
car le pipeline sera créé et géré par GitLab selon une configuration prédéfinie \cite{gitlabautodevops}.

Le fichier YAML contient la liste des tâches qui seront exécutées à chaque fois qu’un pipeline est déclenché 
(cela se produit en général à chaque « push » Git, mais cela peut aussi être une tâche planifiée récurrente).

Un exemple de fichier très simple mais fonctionnel est le suivant \cite{gitlabyaml} :

\begin{lstlisting}[caption=Exemple de fichier .gitlab-ci.yml,,language=yaml]
job1:
  script: "execute-script-for-job1"

job2:
  script: "execute-script-for-job2"
\end{lstlisting}

Ce fichier contient deux tâches (appelées « jobs » dans la documentation), qui seront exécutées indépendamment l’une de l’autre en parallèle. Chaque tâche exécute des commandes précisées avec le mot clé « script ».

De nombreux mots clés sont disponibles et peuvent être utilisés, nous ne les détaillerons pas tous
car cela est déjà bien fait dans la documentation \cite{gitlabyaml} mais on
peut notamment en préciser certains car utiles à la compréhension de la suite du rapport (voir le tableau \ref{tab:keywords}).

\begin{table}
  \caption{Quelques mots clés et leur description}
  \begin{center}
    \begin{tabular}{|p{1in}|p{4in}|}
      \hline
      Mot clé & Description \\
      \hline\hline
      image & Permet d’exécuter une ou plusieurs tâches en utilisant une certaine image Docker.  \\
      \hline
      stage & Un stage est un ensemble de tâches, et le pipeline exécute les stages de manière séquentielle (par exemple, « build », ensuite « test », ensuite « deploy »). Ce mot-clé permet de préciser dans quel stage la tâche devrait s’exécuter. \\
      \hline
      only & Définit la ou les conditions qui doivent être remplies pour que la tâche soit créée (par exemple uniquement quand on effectue une modification sur la branche « master »).  \\
      \hline
      except & Fonctionne de la même façon que le mot-clé « only ».  \\
      \hline
      when & Utile lorsque l’on souhaite déclencher manuellement une tâche lorsque la valeur est définie à « manual ». \\
      \hline
      artifacts & Permet d’envoyer certains fichiers du runner GitLab CI/CD vers GitLab, afin qu’ils puissent être conservés et téléchargés (la durée de conservation peut être définie avec le mot clé "expire\_in").
      Cela nous permet par exemple de renvoyer le résultat des tests unitaires JUnit sous forme de fichier XML afin qu’ils soient affichés sur GitLab. \\
      \hline
      pages & Tâche spéciale qui permet d’envoyer les pages web générées vers GitLab Pages pour qu’elles soient accessibles en ligne. \\
      \hline
  \end{tabular}
  \label{tab:keywords}
  \end{center}
\end{table}

L’écriture du fichier YAML est assez intuitive et l’utilisation du langage YAML facilite beaucoup la lisibilité, tout en permettant une haute flexibilité dans la configuration des différentes tâches.

Un outil appelé « CI Lint » est accessible depuis l’interface web de GitLab et permet de vérifier la syntaxe du fichier avant d’effectuer un commit puis un push.

\subsection{Configuration dans l’interface web}

L’interface web nous permet de configurer certaines options comme : 
\begin{itemize}
  \item L’activation d’Auto DevOps si l’on ne souhaite pas utiliser de fichier .gitlab-ci.yml
  \item La durée maximale d’une tâche (1 heure  par défaut)
  \item La stratégie pour récupérer les derniers changements effectués (Git clone ou Git fetch)
  \item L’expression régulière pour capturer le pourcentage de couverture du code
  \item Les runners à utiliser pour exécuter les tâches
  \item Les variables d’environnement et leur niveau de visibilité
\end{itemize}

Il existe de nombreuses autres options, qui sont dans tous les cas accessibles dans l’onglet « Settings » du projet GitLab.
On remarque que les options de l’interface web sont complémentaires des options dans le fichier .gitlab-ci.yml, mais ne permettent pas de configurer les tâches du pipeline en lui-même (sauf l’option Auto DevOps qui compense l’absence de ce fichier).


\section{Architecture et mode de fonctionnement}


GitLab CI/CD fonctionne de façon simple, le serveur GitLab supervise les tâches qui doivent être exécutées et les attribue à des machines qui vont les exécuter. Ces machines sont appelées « runner ». Un runner est simplement une machine qui fait fonctionner le logiciel GitLab Runner sur une des plates-formes compatibles (toutes celles qui peuvent faire tourner des logiciels écrit en Go).

\begin{figure}[h]
  \begin{center}
  \includegraphics[width=3in]{architecture}
  \end{center}
  \caption{Architecture de GitLab CI}
  \caption*{Source: gitlab.com \cite{gitlabcicd}}
  \label{fig:architecture}
\end{figure}

La figure \ref{fig:architecture} illustre ce principe de fonctionnement, le runner peut très bien être installé sur une machine de travail tout comme sur un serveur. L’inconvénient d’installer le runner sur une machine de travail et que sa disponibilité est compromise lorsque le développeur éteint sa machine. Si l’on souhaite programmer des pipelines en dehors des horaires de travail par exemple, il faudra dans ce cas installer le runner sur un serveur qui aura une meilleure disponibilité. Il est important aussi de noter qu’un runner ne peut pas exécuter des tâches en parallèle, il peut donc parfois être nécessaire d’en avoir plusieurs pour augmenter la vitesse d’exécution globale des pipelines.

Les communications entre le serveur GitLab et les runners se font via une API sur le protocole HTTP, un jeton d’authentification permet au runner de s’authentifier auprès de GitLab pour récupérer les tâches qu’il doit effectuer. Il s’agit d’une architecture client-serveur de base, les runners interrogent le serveur GitLab régulièrement pour savoir s’ils ont du travail à faire.

Il existe des architectures avec de l’auto-scaling où les runners sont automatiquement créés à la demande \cite{gitlabbastion} mais le principe de fonctionnement reste le même que dans le cas de runners créés manuellement.

Pour exécuter des tâches sur un runner, il faut choisir ce que GitLab appelle un « executor ». On a le choix parmi plusieurs comme \cite{gitlabrunner} : Shell, Docker, SSH, Kubernetes, etc.

L’ « executor » définit l’environnement dans lequel les scripts des tâches s’exécutent, si on choisit « Shell », les scripts sont exécutés directement sur la machine hôte, contrairement à Docker où les scripts sont exécutés dans un conteneur (ce qui améliore la sécurité).
